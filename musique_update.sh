#!/bin/bash


testdeps() { # this fonction will check if all the dependencies are installed

    if [ -z $(which mid3v2) ]
    then
        echo you have to install python-mutagen with zypper if you use opensuse
        exit
    fi
    
    if [ -z $(which find) ]
    then
        echo you have to install find with zypper if you use opensuse
        exit
    fi
    
    if [ -z $(which git) ]
    then
        echo you have to install git with zypper if you use opensuse
        exit
    fi

    if [ -z $(which ffmpeg) ]
    then
        echo you have to install ffmpeg with zypper if you use opensuse
        exit
    fi

    if [ -z $(which ffplay) ]
    then
        echo you have to install ffplay with zypper if you use opensuse
        exit
    fi

    if [ -z $(which mp3gain) ]
    then
        echo you have to install mp3gain with zypper if you use opensuse
        exit
    fi

    if [ -z $(which audacity) ]
    then
        echo you have to install audacity with zypper if you use opensuse
        exit
    fi
    
    if [ -z $(which sed) ]
    then
        echo you have to install sed with zypper if you use opensuse
        exit
    fi
    
    if [ -z $(which awk) ]
    then
        echo you have to install awk with zypper if you use opensuse
        exit
    fi

    if [ -z $(which perl) ]
    then
        echo you have to install perl with zypper if you use opensuse
        exit
    fi
}

checknewfiles(){ #this fonction will list all new or modified files and put this list in .newfiles file
    if [ -z $newfilemode ] # si on est en mode par default
    then
        echo on recherche les nouveau fichier avec git
        git status -u --porcelain | grep -v " D " | sed 's/?? //' | sed 's/ M //' | sed -e 's/^\"//g' | sed -e 's/\"$//g' > .newfiles # we ask git to list newfiles and we clean this output ot put it in .newfiles
        echo
    elif [ $newfilemode = "rebase" ] # si on est en mode rebase
    then
        echo on mets a jour tous les fichier
        find -name "*.mp3" | sed -e 's/^.\///' > .newfiles # liste tout les fichier mp3
        echo
    fi
}

cleantmpfiles(){ #cleaning all temp files
    
    echo "c'est l'heure de nettoyer un petit peut. ne t'inquiette pas on s'en occupe"
    rm -f .newfiles
    rm -f .fulllist
    echo

}

rename(){ # this function will remove all spetial char in the name of the files
    echo c\'est parti pour enlever les caractéres pourris que tu à mis dans les nom de tes fichiers.
    while read -r inname; #we loop on all files
    do      
            inname=$(echo $inname | xargs -0 printf) # we repair the accent
            #inname=$(echo $inname | awk '{gsub( /[^A-Za-z0-9./]/, "\\\\&"); print $0}')
            
            outname=$inname # let's make the new name
            outname=$(echo $outname | awk '{print tolower($0)}') # we put all letter to lower case
            outname=$(echo $outname | sed -e 's/ /_/g') # we replace space to underscore
            outname=$(echo $outname | sed -e 's/[^a-z0-9._/]//g') # we remove all char that are not letter number . / or _
            outname=$(echo $outname | sed -e 's/é/e/g' | sed -e 's/è/e/g' | sed -e 's/ê/e/g' | sed -e 's/à/e/g' | sed -e 's/â/e/g' | sed -e 's/ô/o/g' | sed -e 's/ù/u/g' | sed -e 's/û/u/g' | sed -e 's/î/i/g' | sed -e 's/ç/c/g' | sed -e 's/__/_/g') # we remove all accent and correct when there is some double underscore
            echo mv $inname "$outname" # debug
            mv "$inname" "$outname" &> /dev/null # we finaly rename the file
            echo "$outname
            " >> .newfilesrenamed

    done < .newfiles # end of the rename loop
    mv -f .newfilesrenamed .newfiles
}

transcode(){
    echo attention on vas mettre tes musique dans le bon formats
    for infile in $(cat .newfiles) # loop on all the file to convert
    do
            outfile=$(echo $infile | awk -F'.' '{print $1}') # remove extention of file name

            if [ $infile != $outfile ] # si le fichier d'entrée avais une extention
            then
                echo ffmpeg -v 1 -y -i $infile -ar 44100 -ab 112k $outfile\_converted.mp3 #debug
                ffmpeg -v 1 -y -i $infile -ar 44100 -ab 112k $outfile\_converted.mp3 # transcode
                
                rm $infile # remove old file
                mv $outfile\_converted.mp3 $outfile\.mp3 # remove converted in the name of the converted file
                echo "$outfile.mp3
                " >> .newfilesrenamed
            fi

    done
   mv .newfilesrenamed .newfiles 
}

normalizegain(){ # this function will normalize the gain of all music
    
    echo c\'est parti pour normaliser le volume des musique
    for infile in $(cat .newfiles) # loop on all file to normalize
    do

        echo mp3gain -r -c -s s -q  $infile
        mp3gain -r -c -s s -q $infile > /dev/null # calculate and apply the new gain
   
    done
}

settag(){

    echo il est maintenant l\'heure de mettre des tags sur les fichier
    for infile in $(cat .newfiles) # loop on all file to tag
    do
        
        echo mid3v2 -D $infile #remove all tag on file
        mid3v2 -D $infile #remove all tag on file
        
        title=`echo $infile | awk -F'/' '{ print $NF }'` #get the title of the music
        artbum=`echo $infile | sed -e "s/$title//"` #remove the title of the path to the file to get de artist and album tag
        title=$(echo $title | awk -F'.' '{print $1}') #remove extention of the title

        echo mid3v2 -a $artbum -A $artbum -t $title $infile  #put the album, artist and title tag  
        mid3v2 -a $artbum -A $artbum -t $title $infile       #put the album, artist and title tag  
      
        echo
    done
    
}

addtogit(){
    
    echo c\'est la derniere etapes, on ajoute toute les modifications au depot git
    #echo "\"musique :" > .fulllist
    for infile in $(cat .newfiles) # loop on all file to normalize
    do

        echo git add $infile   
        git add $infile   
        echo \-$infile\n >> .fulllist
        #fulllist=$(echo $fulllist -$infile)
    done
    echo
    echo ajoutée ou mise a jour\" >> .fulllist
    echo git commit -m "$(cat .fulllist)"
    git commit -m "$(cat .fulllist)"
}

    
newfilemode=""

case "$1" in # tes des paramettres
    
    -h|--help)     
        echo Ceci est un script permettant de rajouter proprement dans musique dans la base de musique servant principalement pour la voiture.
        echo voici les option a votre disposition
        echo --rebase permet d\'effectuer toute les action normalement reservée au nouveaux fichier sur tout les fichiers en .mp3
        echo -f fichier1 fichier2 permet de saisir une list de fichier a transformer
        exit
    ;;
        
    -rebase|--rebase)
        echo on fait un rebase
        newfilemode="rebase"
        echo
    ;;
  
    -f)
        echo on passe en mode liste de fichier
        cleantmpfiles
        echo
        echo voici la liste :
        newfilemode="listed"
        #echo $1
        while [ ! -z $1 ]
        do
            #echo boucle
            shift
            echo $1
            echo "$1
            " >> .newfiles
        done   
        echo
    ;;
esac


testdeps

checknewfiles
rename
echo
echo
transcode
echo
echo
normalizegain
echo
echo
settag
addtogit
cleantmpfiles

